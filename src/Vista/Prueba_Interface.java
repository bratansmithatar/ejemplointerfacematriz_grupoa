/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Interface.*;
import Negocio.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class Prueba_Interface {
    
    public static void main(String[] args) {
        
          try {
            System.out.println("Por favor digite datos de la matriz:");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas=lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols=lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini=lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin=lector.nextInt();
            
        
            
            // :)
            Matriz_Aleatorio myMatriz=new Matriz_Aleatorio(filas, cols);
            myMatriz.crearElementos_Aleatorios(ini, fin);
            System.out.println(myMatriz.toString());
            
            //Probando nuestra Interface IOperacion:
            //FORMA 1: (objetos de la clase e invocando sus métodos)
            System.out.println("Forma 1:"+myMatriz.getTotal());
              
            //FORMA 2: (POLIMORFISMO)
            IOperacion interface1=myMatriz; //casting
            System.out.println("Forma 2:"+interface1.getTotal());
          
            //FORMA 3: (CASTING DE LA INTERFACE)
            int total=((IOperacion)myMatriz).getTotal();
            System.out.println("Forma 3:"+total);
          
            //Con el objeto tripleta:
            Tripleta tripleta=new Tripleta(3,6,9);
            interface1=tripleta;
            System.out.println("Forma 2-tripleta:"+interface1.getTotal());
           
            //Con el objeto Vector_aleatorio:
            Vector_Aleatorio myVector = new Vector_Aleatorio(10);
            interface1=myVector;
            System.out.println("Forma 2-vector_aleatorio:"+interface1.getTotal()); 
          
            System.out.println("Probando nuestra Interface IOperacion2:");
            //FORMA 1: (objetos de la clase e invocando sus métodos)
            myMatriz.interCambio();
            System.out.println("Matriz forma 1: " + "\n" + myMatriz.toString());
              
            //FORMA 2: (POLIMORFISMO)
            IOperacion2 interface2 = myMatriz; //casting
            interface2.interCambio();
            System.out.println("Matriz Forma 2: "+ "\n" + interface2.toString());
          
            //FORMA 3: (CASTING DE LA INTERFACE)
            ((IOperacion2)myMatriz).interCambio();
            System.out.println("Matriz forma 3: "+ "\n" + ((IOperacion2)myMatriz).toString());
            
            //FORMA 1: (objetos de la clase e invocando sus métodos)
            tripleta.interCambio();
            System.out.println("Tripleta forma 1: " + tripleta.toString());
              
            //FORMA 2: (POLIMORFISMO)
            IOperacion2 interface3 = tripleta; //casting
            interface3.interCambio();
            System.out.println("Tripleta Forma 2: "+interface3.toString());
            
            //FORMA 3: (CASTING DE LA INTERFACE)
            ((IOperacion2)tripleta).interCambio();
            System.out.println("Tripleta forma 3: "+((IOperacion2)tripleta).toString());
            
            //FORMA 1: (objetos de la clase e invocando sus métodos)
            myVector.interCambio();
            System.out.println("Vector forma 1: "+myVector.toString());
              
            //FORMA 2: (POLIMORFISMO)
            IOperacion2 interface4=myVector; //casting
            interface4.interCambio();
            System.out.println("vector Forma 2: "+interface4.toString());
            
            //FORMA 3: (CASTING DE LA INTERFACE)
            ((IOperacion2)myVector).interCambio();
            System.out.println("vector forma 3: "+((IOperacion2)myVector).toString());
            } 
            catch(java.util.InputMismatchException ex2)
            {
              System.err.println("Error en la entrada de datos enteros:"+ex2.getMessage());
            }
            catch (Exception ex) {
                  // :(
               System.err.println("Error:"+ex.getMessage());
            }       
    }  
}
