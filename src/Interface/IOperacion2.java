/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

/**
 *
 * @author madar
 */
public interface IOperacion2 {
    /**
     * Para la clase Matriz_Aleatorio: cambia cada vector de la fila por su inverso:
     *   Ej: 
     *          m=| 3 4 5 |   --> m
     *            |13 6 7 |  
     * 
     *          m=| 5 4 3 |   --> m
     *            |7 6 13 |  
     * 
     * Para la clase Vector_Aleatorio
     * *          V=| 3 4 5 |   --> | 5 4 3 |
     * 
     * Para la tripleta: num1 pasa a num2, num2 pasa a num3 y num3 pasa a num1
     */
    public void interCambio();
}
